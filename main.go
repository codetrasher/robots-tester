// robots-tester project main.go
package main

import (
	//	"bufio"
	"fmt"
	"io/ioutil"
	"net/http"
	//	"os"
)

const ROBOTS string = "robots.txt"
const HUMANS string = "humans.txt"
const USER_AGENT_CONTENT string = "robots.txt-finder/1.0.0 (This application is used ONLY for testing purposes.)"

func main() {

	//	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter URL to test: ")
	//	url, _ := reader.ReadString('\0')
	var url string
	fmt.Scanln(&url)

	fmt.Println("================================")
	fmt.Println("Searching for robots.txt...")
	checkTxt("http://" + url + "/" + ROBOTS)
	fmt.Println("================================") /*
		fmt.Println("Searching for humans.txt...")
		checkTxt("http://" + url + "/" + HUMANS)
		fmt.Println("================================")*/
}

func checkTxt(url string) {

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err)
	}
	req.Header.Set("User-Agent", USER_AGENT_CONTENT)
	req.Header.Set("Content-Type", "text/html")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)

	fmt.Printf("Response: %s\n", string(b))
}
